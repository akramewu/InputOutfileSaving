﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        static bool IsPrimeNumber(int num)
        {
           bool bPrime = true;
           int factor = num/2;

           int i;
           for(i=2;i<=factor;i++)
           {
              if((num%i)==0)
              bPrime=false;
           }
           return bPrime;
        }

        
        
        public static void Main(string[] args)
        {
           Console.WriteLine("List of prime numbers ");
           int userInput = Convert.ToInt32(Console.ReadLine());
           for(int i =2; i<userInput; i++)
           {
               if(IsPrimeNumber(i)== true)
               Console.WriteLine("{0,8:n}\t",i);
           }
            Console.ReadLine();
        }
    }
}
